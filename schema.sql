DROP TABLE IF EXISTS `participants`;
CREATE TABLE `participants` (
    id integer primary key autoincrement,
    name text not null unique,
    password varchar(255)
);


DROP TABLE IF EXISTS `participant_lists`;
CREATE TABLE `participant_lists` (
    id integer primary key autoincrement,
    name text not null unique
);


DROP TABLE IF EXISTS `list_participants`;
CREATE TABLE `list_participants` (
    participant_id integer not null,
    list_id integer not null
);

DROP TABLE IF EXISTS `pools`;
CREATE TABLE `pools` (
    id integer primary key autoincrement,
    owner integer not null, -- participant id
    fill integer not null -- the value currently held by this pool
);


DROP TABLE IF EXISTS `taglines`;
CREATE TABLE `taglines` (
    id integer primary key autoincrement,
    text text not null
);


DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions` (
    id integer primary key autoincrement,
    initiator integer not null, -- FRBRZTL, participant id
    recipient integer not null, -- participant id of the recipient
    value integer not null,
    message text not null,
    url text
);
