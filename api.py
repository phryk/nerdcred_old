from flask import Blueprint

api = Blueprint('api', __name__)

@api.route('/')
def discover():
    return 'discover goes here'
