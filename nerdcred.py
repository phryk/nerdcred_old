#import pgdb #TODO: port to postgres, like whenever
from os.path import exists
import sqlite3
import config

from threading import Thread
from hashlib import new as mkhash



db_container = None


# Exceptions below this

class ResourceError(Exception):

    pass


class DatabaseError(Exception):

    pass


# auxiliary functions below this

def db_connect():

    if exists(config.DATABASE):
        db_con = sqlite3.connect(config.DATABASE)
        db_container.db_con = db_con
        return db_con

    else:
        raise Exception('Database not found: %s' % (config.DATABASE))


def db_query(query, args=(), one=False):
    cur = db_container.db_con.execute(query, args)
    rv = [dict((cur.description[idx][0], value)
            for idx, value in enumerate(row)) for row in cur.fetchall()]

    return (rv[0] if rv else None) if one else rv



# Threading stuff below this

class SingletonMeta(type):

    """Singleton Metaclass"""

    _instances = {}

    def __call__(cls, *args, **kwargs):

        if cls not in cls._instances:
            cls._instances[cls] = super(SingletonMeta, cls).__call__(*args, **kwargs)

        return cls._instances[cls]



class Spiderman(Thread):

    __metaclass__ = SingletonMeta

    daemon = True # make this a daemon thread

    values = None


    def __init__(self):

        super(self.__class__, self).__init__()

        self.values = {}
        self.threads = {}

    def __getitem__(self, name):

        try:
            return self.values[name]

        except: raise

    def __setitem__(self, name, value):

        try:
            return self.values[name]

        except: raise


    def interval(self):

        return 42

    def run(self):

        pass # run thread stuff here



class participant(object):

    _in_db = None
    id = None
    name = None
    password = None
    pools = None
    taglines = None


    def __init__(self, id=None, name=None):

        _in_db = False
        ident = 'unknown'
        load = False
        r = []

        if id:
            ident = id
            load = True
            r = db_query('SELECT * FROM `participants` WHERE id = ?', [id], True)

        elif name:
            ident = namie
            load = True
            r = db_query('SELECT * FROM `participants` WHERE name = ?', [name], True)


        if r is not None and len(r):
            self.id = id
            self.name = r['name']
            self._in_db = True

        elif load:
            raise ResourceError("Could not load %s '%s'." % (self.__class__.__name__, ident))


    def save(self):

        if not self.name:
            raise ResourceError("Could not save %s because name is not set." % (self.__class__.__name__))

        elif self._in_db:

            query = 'UPDATE `participants` SET name = ?'
            params = [self.name]

            if self.password:
                query +=', password = ?'
                params.append(self.password)

            db_query(query, *params)

        else:

            if not self.password:
                raise ResourceError("Could not create %s '%s' because no password is set." % (self.__class__.__name__, self.name))

            pwhash = mkhash(config.PASSWORD_ALGORITHM)
            pwhash.update(config.SALT_SECRET + self.name + self.password)
            db_query('INSERT INTO `participants` (name, password) VALUES (?, ?)', self.name, pwhash.hexdigest())

            #TODO: Create pool(s) and participant list(s)

            self._in_db = True


class participant_list(object):

    id = None
    name = None
    participants = None
    taglines = None


    def __init__(self, id=None, name=None):

        ident = 'unknown'

        if id:
            ident = id
            r = db_query('SELECT * FROM `participant_lists` WHERE id = ?', [id], True)

        elif name:
            ident = name
            r = db_query('SELECT * FROM `participant_lists` WHERE name = ?', [name], True)
            
        if len(r):
            self.id = id
            self.name = r['name']

        else:

            raise ResourceError("participant_list '%s' could not be loaded." % (str(ident)))


    def save(self):

        pass



class creator(Thread): # not in database, only created via API
    
    id = None
    name = None


    def __init__(self, id=None, name=None):

        pass


    def run(self):

        pass 



class pool(object):

    id = None
    owner = None
    balance = None


    def __init__(self, id=None, name=None):

        pass


    def save(self):

        pass


class transaction(object):

    sender = None
    recipient = None
    value = None


    def __init__(self, id=None, name=None):

        pass


    def save(self):

        pass
