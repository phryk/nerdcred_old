from flask import Flask, Blueprint, g
from api import api
from wui import wui


app = Flask(__name__)
app.config.from_pyfile('app_config.py')


app.register_blueprint(api, url_prefix='/api')
app.register_blueprint(wui, url_prefix='/wui')

if __name__ == '__main__':
    app.run()
