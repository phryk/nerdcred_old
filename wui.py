from flask import Blueprint, render_template, g
import nerdcred


class renderable(object):

    def render(self, mode = 'view'):

        return render_template('%s-%s.jinja' % (self.__class__.__name__, mode), obj = self)


class participant(nerdcred.participant, renderable):

    pass


class participant_list(nerdcred.participant_list, renderable):

    pass


class creator(nerdcred.creator, renderable):

    pass


class pool(nerdcred.pool, renderable):

    pass


class transaction(nerdcred.transaction, renderable):

    pass


nerdcred.db_container = g

wui = Blueprint('wui', __name__, template_folder='templates')

@wui.before_request
def init():

    nerdcred.db_connect()

@wui.route('/')
def home():

    u = participant(1)

    return u.render()


@wui.route('/participant/<int:id>/')
@wui.route('/participant/<name>/')
def participant_view(id = None, name = None):

    if id:
        p = participant(id)

    elif name:
        p = participant(name = name)

    return p.render()


@wui.route('/list/<int:id>/')
@wui.route('/list/<name>/')
def list_view(id = None, name = None):

    if id:
        l = participant_list(id)

    elif name:
        l = participant_list(name = name)

    return l.render()


@wui.route('/creator/<int:id>/')
@wui.route('/creator/<name>/')
def creator_view(id = None, name = None):

    if id:
        c = creator(id)

    elif name:
        c = creator(name = name)

    return c.render()


@wui.route('/pool/<int:id>/')
@wui.route('/pool/<name>/')
def pool_view(id = None, name = None):

    if id:
        p = pool(id)

    elif name:
        p = pool(name = name)

    return p.render()


@wui.route('/transaction/<int:id>/')
@wui.route('/transaction/<name>/')
def transaction_view(id = None, name = None):

    if id:
        t = transaction(id)

    elif name:
        t = transaction(name = name)

    return t.render()
