# Nerdcred - something like a specification

Something something introduction…


## Rough description

*Nerdcred* is a shorthand of 'nerd credibility'.
It is a peculiar flavour of online game supposedly intended to assign a
quantifiable measure of 'nerdiness' to whatever entity chooses to
participate.

That it might provide a nice playground for experiments in programming
and mind control is merely a completely unintentional side effect.




## Data, behavior and interactions

### <a name="points"></a>Points


Points are always represented as integers, with an atomic value of 1 point.


### <a name="participants"></a>Participants

A participant is any entity that chooses to participate in a nerdcred economy.
Usually this is a human, or a similarly sentient entity. Bots are certainly
possible but it is not yet known if they could or would contribute to the
game dynamics.

A participant holds an id as well as a name and a password. In the far future,
the password might be replaced by some fancy key/challenge authentication thing.


### <a name="transactions"></a>Transactions

Participants can assign points to other participants in an ordered fashion;
This is called a transaction. 

This document refers to the act of sending a transaction to a participant as
'rating' that participant.

A transaction holds references to its initiator and recipient as well as a
value in points, which might be positive or negative.


#### <a name="transaction-authorization"></a>Transaction authorization

Rating a participant requires for the initiator of the transaction to be
authorized by the recipient.

While given by the (potential) recipient, the authorization can be requested
by any participant who wishes to rate said recipient.

This transaction authorization is non-mutual. That means *participant A* can
authorize *participant B* to rate them without *participant B* having to
authorize transactions from *participant A* in return.


### <a name="pools"></a>Pools

Each participant is assigned one pool.

In the future, multiple pools might be available, but since the exact mechanics
would have to be established first, each participant only has one for now.

A participants pools determine the maximum absolute value a participant can
put into a transaction. That means that if a participant has one pool which
holds a value of 5, they can conduct transactions with a value between
-5 and 5 points.

When a transaction is conducted, the absolute value of the transaction is
subtracted from the value of the pool. If the absolute value of the transaction
is greater than the value of the pool, the transaction is rejected.

A pool holds a reference to its owner and [creator](#creators) as well as its absolute value in points.


### <a name="creators"></a>Creators

Creators are the entities that fill up the participants pools.
They are the mechanism that *creates* points.

The first creator is called Spiderman.
Because reasons. Because `class Spiderman(Thread)`. Do not question this.

Every [SPIDERMAN_INTERVAL](#config-vars) seconds, the 'Spiderman' creator
adds 1 point to every pool that holds less than [SPIDERMAN_MAX](#config-vars)
points.


### <a name="score"></a>Score, filtering

A score is the sum of all of a participants transactions, optionally given
a set of rules to filter by. The default filter set that is used to display
a participants score can be defined by the participants themselves.

Other filter rules might be used to research the nerd credibility of a
participant within certain context, like for instance within a certain
[participant list](#participant-lists).

At the beginning, only filtering by [participant lists](#participant-lists) will
be possible, but more filtering traits will be implemented if deemed necessary.


### <a name="participant-lists"></a>Participant lists

UPDATE THIS

Participants can add others participants to a list if the contactee
authorizes a contact request sent from the participant seeking contact.

Multiple lists are possible and encouraged for group building.
Participant lists can be public, private, or invite-only.

There is one participant list automatically generated for each
participant , this is the default contact list of the participant.
A common use for this contact list would be keeping the direct
contacts and immediate social nerd environment of the participant.

To further enable group building, participants can join a publicized
contact list. By default a new list member has to request authorization from
each contact list member. To keep a constant stream of contact requests from
happening, participants can choose to grant or deny requests from specific
groups automatically.

With this, a participant list can also be used as a blacklist in order to block
request flooding from known malicious participants.

Visibility: admin|members|public



## Extra stuff

### <a name="tag-lines"></a>Tag lines

Tag lines are … TBD

### <a name="achievements"></a>Achievements 

Nerd license at `x` points.
Different sizes of nerd licenses.


### <a name="config-vars"></a>Configuration variables

    * PASSWORD_ALGORITHM = 'sha512' # The hash algorithm used for storing passwords *DONTCHANGE
    * SALT_SECRET = 'foobar' # Part of the salt used for hashing *DONTCHANGE
    * SPIDERMAN_MAX = 5
    * SPIDERMAN_INTERVAL = 3600 # 1h

*DONTCHANGE* - Changing any of these variables in a non-empty environment will fuck things up badly.


## <a name="implementation-organization"></a>Implementation organization

The implementation is organized in two pieces - the main nerdcred library/API
and the web application.

The web application itself is organized in several smaller pieces, too.
These are a REST API and the default nerdcred web-client. A component to
also provide the API via websockets might follow.


## <a name="database"></a>Database

UPDATE OR DELETE THIS

Cache scores instead of recalculating for each request?
Not in the database, caching should be done somewhere else!
If caching becomes required out of performance reasons,
just make a dictionary holding the <n> most viewed
participant/filter combinations' transaction sums.

users
    id (int, autoincrement, primary key)
    name (varchar/text)
    pass (varchar/text, sha-256/whatev)


transactions
    initiator (int, user id)
    recipient (int, user id)
    value (int)
    time (int/datetime)



list_info #contact list info
    name
    owner
    label
    description


list_members
    name #reference to list_info's name
    user
    authorize (varchar/text, grant|*interactive*|deny)
    visibility (varchar/text, private|*members*|public)
